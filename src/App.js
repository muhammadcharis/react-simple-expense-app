import ExpenseItem from "./components/ExpenseItem";
import ExpenseButtonNew from "./components/ExpenseButtonNew";

import { useState } from "react";
function App() {
  const [expenses, setExpanses] = useState([
    {
      id: "id1",
      expenseDate: new Date(2021, 2, 28),
      expenseTitle: "Car Insurance",
      expenseAmount: 1000,
    },
    {
      id: "id2",
      expenseDate: new Date(2022, 2, 28),
      expenseTitle: "Dental Insurance",
      expenseAmount: 200,
    },
  ]);

  const saveExpenseHandler = (data) => {
    const newData = {
      id: Math.random().toString(),
      ...data,
    };

    //why cant update ?
    //console.log(expenses);
    //expenses.unshift(newData);
    //setExpanses(expenses);

    //console.log(expenses);
    setExpanses((prev) => {
      return [newData, ...prev];
    });
  };

  return (
    <>
      <ExpenseButtonNew onSaveData={saveExpenseHandler} />
      <ExpenseItem expenses={expenses} />
    </>
  );
}

export default App;
