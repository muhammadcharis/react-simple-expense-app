import React from "react";

const ExpenseDate = (props) => {
  const day = props.date.toLocaleDateString("en-US", {
    day: "numeric",
  });
  const month = props.date.toLocaleDateString("en-US", {
    month: "long",
  });
  const year = props.date.toLocaleDateString("en-US", {
    year: "numeric",
  });

  return (
    <div className="flex text-white flex-col w-20 h-20 border border-white bg-[#2a2a2a] rounded-lg items-center justify-center">
      <div className="font-semibold text-lg">{month}</div>
      <div className="text-xs">{year}</div>
      <div className="font-semibold text-lg">{day}</div>
    </div>
  );
};

export default ExpenseDate;
