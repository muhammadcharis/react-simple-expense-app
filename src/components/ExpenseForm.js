import React, { useState } from "react";

const ExpenseForm = (props) => {
  const [title, setTitle] = useState("");
  const [amount, setAmount] = useState("");
  const [date, setDate] = useState("");

  const submitHandler = (e) => {
    e.preventDefault();
    if (title && amount && date) {
      const formDate = {
        expenseTitle: title,
        expenseAmount: parseInt(amount),
        expenseDate: new Date(date),
      };

      setTitle("");
      setAmount("");
      setDate("");
      props.onSaveData(formDate);
    }
  };

  return (
    <div className="new-expense">
      <div className="new-expense__actions">
        <form
          onSubmit={submitHandler}
          className="flex flex-col justify-center items-center">
          <div className="new-expense__controls">
            <div className="new-expense__control">
              <label>Date</label>
              <input
                type="date"
                value={date}
                min="2019-01-01"
                max="2022-12-31"
                onChange={(e) => setDate(e.target.value)}
              />
            </div>
          </div>

          <div className="new-expense__controls">
            <div className="new-expense__control">
              <label>Title</label>
              <input
                type="text"
                name="title"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
            </div>
          </div>
          <div className="new-expense__controls">
            <div className="new-expense__control">
              <label>Amount</label>
              <input
                type="number"
                name="amount"
                min="0.01"
                step="0.01"
                value={amount}
                onChange={(e) => setAmount(e.target.value)}
              />
            </div>
          </div>
          <div className="new-expense__actions">
            <button
              type="button"
              onClick={() => props.onCancelForm()}
              className="bg-red-500 !impo">
              Cancel
            </button>
            <button type="submit">Add Expense</button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default ExpenseForm;
