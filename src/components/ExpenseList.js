import React from "react";

import Card from "../components/Card";
import ExpenseDate from "../partials/expanse-item/ExpenseDate";

const ExpenseList = (props) => {
  if (props.data.length === 0)
    return (
      <div className="text-white font-bold flex justify-center ">
        <p>Data not found</p>
      </div>
    );

  return (
    <>
      {props.data.map((row, index) => {
        return (
          <Card key={row.id} className="expense-item">
            <ExpenseDate date={row.expenseDate} />
            <div className="expense-item__description">
              <h2>{row.expenseTitle}</h2>
              <div className="expense-item__price">${row.expenseAmount}</div>
            </div>
          </Card>
        );
      })}
    </>
  );
};

export default ExpenseList;
