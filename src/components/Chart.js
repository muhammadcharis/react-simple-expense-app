import React from "react";
import ChartBar from "./ChartBar";

const Chart = (props) => {
  const dataPointValues = props.dataPoints.map(
    (dataPoints) => dataPoints.value,
  );

  const totalMax = Math.max(...dataPointValues);

  console.log(props.dataPoints);
  return (
    <div className="chart">
      {props.dataPoints.map((row) => {
        return (
          <ChartBar
            key={row.label}
            label={row.label}
            value={row.value}
            max={totalMax}
          />
        );
      })}
    </div>
  );
};

export default Chart;
