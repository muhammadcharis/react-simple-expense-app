import React, { useState } from "react";

import Card from "../components/Card";
import ExpenseFilter from "../components/ExpenseFilter";
import ExpenseList from "../components/ExpenseList";
import ExpenseChart from "../components/ExpenseChart";

const ExpenseItem = (props) => {
  const [filterYear, setFilterYear] = useState("");

  const saveSelectedYear = (value) => {
    setFilterYear(value);
  };

  const dataExpenses = filterYear
    ? props.expenses.filter(
        (row) => row.expenseDate.getFullYear().toString() === filterYear,
      )
    : props.expenses;

  console.log(dataExpenses);
  return (
    <Card className="expenses">
      <ExpenseFilter onSelectedFilter={saveSelectedYear} />
      <ExpenseChart dataPoints={dataExpenses} />
      <ExpenseList data={dataExpenses} />
    </Card>
  );
};

export default ExpenseItem;
