import React from "react";
import Chart from "./Chart";

const ExpenseChart = (props) => {
  const dataPoints = [
    { label: "January", max: 0, value: 0 },
    { label: "February", max: 0, value: 0 },
    { label: "March", max: 0, value: 0 },
    { label: "April", max: 0, value: 0 },
    { label: "May", max: 0, value: 0 },
    { label: "June", max: 0, value: 0 },
    { label: "July", max: 0, value: 0 },
    { label: "August", max: 0, value: 0 },
    { label: "September", max: 0, value: 0 },
    { label: "October", max: 0, value: 0 },
    { label: "November", max: 0, value: 0 },
    { label: "December", max: 0, value: 0 },
  ];

  props.dataPoints.forEach((el) => {
    const getMonth = el.expenseDate.getMonth();
    dataPoints[getMonth].value += el.expenseAmount;
  });

  console.log(dataPoints);
  return <Chart dataPoints={dataPoints} />;
};

export default ExpenseChart;
