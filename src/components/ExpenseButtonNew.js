import React, { useState } from "react";

import ExpenseForm from "../components/ExpenseForm";

const ExpenseButtonNew = (props) => {
  const [showFrom, setShowForm] = useState(false);
  const showFormHandler = () => {
    setShowForm(true);
  };

  const saveExpenseHandler = (data) => {
    props.onSaveData(data);
  };

  const hideForm = () => {
    setShowForm(false);
  };

  if (!showFrom)
    return (
      <div className="new-expense">
        <button type="button" onClick={showFormHandler}>
          Add New Expense
        </button>
      </div>
    );

  return (
    <ExpenseForm onSaveData={saveExpenseHandler} onCancelForm={hideForm} />
  );
};

export default ExpenseButtonNew;
