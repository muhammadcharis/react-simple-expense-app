import React from "react";

const Card = (props) => {
  return (
    <div className={["card", props.className].join(" ")}>{props.children}</div>
  );
};

export default Card;
